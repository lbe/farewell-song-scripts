# Farewell song script

This script is used to automatically generate a video with the associated photos and lyrics for the traditional LBE farewell drink

## Lyrics

Try to find the .lrc file of the original song with your favourite search engine. This contains the lyrics and the timecode to sync them.

If the song is not very famous maybe you won't find it, so you can use this website to easilly generate it semi-manually :
http://lrcgenerator.com/

Then, convert the .lrc file to a .srt file using this website :
https://toolslick.com/conversion/subtitle/lrc-to-srt

Finally, you can edit this file (with Notepad, Atom...) and replace the original lyrics with yours :)


## Video

Put all the pictures in the "pics" folder.

Put the song in .mp3 in the main folder (same folder as the python script) and call it "song.mp3"

If you want some pictures to appear at specific timepoints in the video, you should fill the picName column in the "picSelection_input" file that will be generated at the first execution. If you add or remove pictures from the "pics" folder, you should delete the "picSelection_input" file so a newer one with the correct amount of rows is generated.

If you want to burn the lyrics into the video, open a terminal window and go to the folder containing the video and the srt file, and run: (after having installed ffmpeg)
"ffmpeg -i video.mp4 -vf subtitles=video.srt video_srt.mp4 -b:a 256k"
(however this seems to reduce the sound quality)

## Troubleshooting

If you put some picture names in the 'picSelection_input.xlsx' file, make sure that they contain the format (.jpg, .png...) at the end

If you have black bands on the top and bottom of your video, this means at least one of the pictures is too wide and you should crop it

If you are getting an "TypeError: must be real number, not NoneType" error when running "clip.write_videofile" command, try upgrading the moviepy package ("pip install moviepy --upgrade")
