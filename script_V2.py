# Author : malo sanglier 
# Purpose : Generate a video with associated photos and lyrics
# Date : 02/12/2022


from moviepy.editor import *
from PIL import Image, ImageOps
from os import listdir
import time
import numpy as np
import pandas as pd

files = listdir("./pics")
files.remove("resized")
files = pd.Series(files)
song = AudioFileClip('song.mp3')
video_length = song.duration

try:
    picSelection = pd.read_excel("picSelection_input.xlsx")
except:
    pic_number = len(files)
    pic_length = video_length/pic_number
    picSelection = pd.DataFrame(columns=["clipStart", "clipEnd", "picName"])
    pic_range = np.arange(0, video_length+pic_length, pic_length)
    pic_range = [time.strftime("%M:%S", time.gmtime(element)) for element in pic_range]
    picSelection.clipStart = pic_range[:-1]
    picSelection.clipEnd = pic_range[1:]
    picSelection.to_excel("picSelection_input.xlsx", index=False)
    input("If you want some pictures to appear at a specific timepoint, add their name in the 'picSelection_input.xlsx' file NOW\nThen hit enter")
    picSelection = pd.read_excel("picSelection_input.xlsx")

manuallySelectedPics = pd.Series(picSelection.picName.unique()).dropna()
notManuallySelectedPics = pd.Series(list(set(files) - set(manuallySelectedPics))).sample(frac=1) # the sample attribute randomizes the order of the rows
picSelection.loc[picSelection.picName.isna(), "picName"] = list(notManuallySelectedPics)
picSelection.to_excel("picSelection_output.xlsx")
height = 1080

def resizeImage(f, fixed = None, basewidth = None, baseheight = None): #need to fix problem with rotated pics
    img = Image.open(f)
    img = ImageOps.exif_transpose(img) #transposes image the right way (portrait or landscape)
    if fixed:
        img = img.resize((fixed[0], fixed[1]), Image.ANTIALIAS)
    elif basewidth:
        wpercent = (basewidth / float(img.size[0]))
        hsize = int((float(img.size[1]) * float(wpercent)))
        img = img.resize((basewidth, hsize), Image.ANTIALIAS)
    elif baseheight:
        hpercent = (baseheight / float(img.size[1]))
        wsize = int((float(img.size[0]) * float(hpercent)))
        img = img.resize((wsize, baseheight),Image.ANTIALIAS)
    return img

pics = [ImageClip(f, duration = video_length/len(files)) for f in "pics/resized/" + picSelection.picName]

clip = concatenate_videoclips(pics, method = "compose")
clip = clip.set_audio(song)
clip.write_videofile("video.mp4", fps=24)
